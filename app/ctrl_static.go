package app
/*
import (
      
      "net/http"
     // "bytes"
        "net/url"

        "os"
        "fmt"
      "io"
         // "appengine/datastore"
            //"appengine"
       //"io/ioutil"
        "strings"

        //HAS ISSUES
        "cloud.google.com/go/storage"
        //"golang.org/x/net/http2"
        
        //GOOD     
        "golang.org/x/net/context"
        //"golang.org/x/oauth2/google"
        //"golang.org/x/oauth2/internal"
        //"google.golang.org/appengine/internal/datastore"
        "google.golang.org/appengine/datastore"

       //"google.golang.org/api/datastore"

        //"google.golang.org/api/iterator"
        "google.golang.org/appengine"
        //"google.golang.org/appengine/file"
        "google.golang.org/appengine/log"
        //"google.golang.org/cloud/storage"

)


  type cloudItem struct {

  client     *storage.Client
  bucketName string
  bucket     *storage.BucketHandle

  w   io.Writer
  ctx context.Context
  // cleanUp is a list of filenames that need cleaning up at the end of the demo.
  cleanUp []string
  // failed indicates that one or more of the demo steps failed.
  failed bool
}

func staticGenerator(w http.ResponseWriter, r *http.Request){

	//ADMIN
  checkAdmin(w,r)


  //CONTEXT
  ctx := appengine.NewContext(r)

  /*  //BUCKET NAME
  bucketName, err := file.DefaultBucketName(ctx); 
  
  if err != nil {
      log.Errorf(ctx, "failed to get default GCS bucket name: %v", err)
      fmt.Fprintln(w,"error with making upload URL on GET:" + err.Error())
      return
  }*/
/*
  //CLIENT
   client, err := storage.NewClient(ctx)
  if err != nil {
    log.Errorf(ctx, "failed to create client context: %v", err)
    return
  }

  //BUCKET NAME
  globalBucketName := os.Getenv("GCLOUD_STORAGE_BUCKET")

  //BUCKET
  bkt := client.Bucket(globalBucketName)



  

    
  //ATTRIBUTES
  attrs,err := bkt.Attrs(ctx)

    if err != nil {
      log.Errorf(ctx, "failed to get bucket attributes  : %v", err)
      fmt.Fprintln(w,err)
      return
    }

//c,_ :=  getCloudContext(r)

    //########################GET##################################//
  if r.Method == "GET"{


    q := datastore.NewQuery("page").Limit(100)


    //DB GET GetAll
    var db []*Page
    keys,err := q.GetAll(ctx,&db)

    //DB ERR
    if err != nil {
      fmt.Fprint(w,"error getting pages")
      return
    }

  
     var dbCore map[string]string
     dbCore = getCore(w,r)

    //VAR
    var dbData []map[string]string



  //FOR DB ITEMS
    for i := range db {
      
      k := keys[i].Encode()

      dbData = append(dbData,
        map[string]string{
        "key":k,   
         "title": db[i].Title,
         "content":db[i].Content,
         "description": db[i].Description,
         "slug":db[i].Slug,
         "single":db[i].Single,
         "header":dbCore["CoreHeader"],
         "footer":dbCore["CoreFooter"],
       },
      )
    }
    




/*
  acc, _ := appengine.ServiceAccount(ctx)

  httpClient, err := google.DefaultClient(ctx, storage.ScopeFullControl)
     if err != nil {
            log.Errorf(ctx, "failed to get default client  : %v", err)
            fmt.Fprintln(w,err)
            return
        }
*/




    //QUERY
  
      //BUFFER
      //buf := &bytes.Buffer{}
    
    /*
      //OBJECT
      cloud := &cloudItem{
        w:          buf,
        ctx:        ctx,
        client:     client,
        bucket:     client.Bucket(bucketName),
        bucketName: bucketName,
      }*/
/*

      //DATA
     data := map[string]string{
       "get":"true",
       "location":attrs.Location,
       "storageClass":attrs.StorageClass,
       "bucket_name":attrs.Name,
       "global_bucket_name": globalBucketName,
       "title": "Static Generator",
     } 





        //log.Fprintln(w,data)
        //log.Debugf(ctx, "",data)
/*
      
      //DEBUG 
      fmt.Fprintln(w, staticHTML)

        fmt.Fprintln(w,client)
        fmt.Fprintln(w,bkt)
        fmt.Fprintln(w,attrs)
        fmt.Fprintln(w,data)
        fmt.Fprintln(w,cloud)
        //fmt.Fprintln(w,httpClient)
        //fmt.Fprintln(w,acc)
*/



   /*

       //RENDER
   renderControl(w, r, "/control/ctrl_static.html", data, dbData)
  
  //END GET 
   }


//########################POST##################################//
if r.Method == "POST"{

  //PARSE URL
  realURL,_ := url.Parse(r.RequestURI)
  url := realURL.String()
  splitURL := strings.Split(strings.Trim(url,"/"),"/")

          log.Debugf(ctx, "",url)
          log.Debugf(ctx, "",splitURL)

  //GET PAGE DATA
  pageData := getPageWithKey(w,r,splitURL[3])

  //RENDER STATIC PAGE
  staticHTML := renderStatic(w, r,"core/page.html", pageData,nil)



  slug := pageData["url"]
  theFileName := ""



  if slug == "/" {
    theFileName =  "homepage.html"
  } else {
    theFileName = strings.Trim(slug, "/") +".html"
  }


  fmt.Fprintln(w,"Checking for previous file...")



  //CHECK FOR PREVIOUS FILE
  objAttrs,_ := bkt.Object(theFileName).Attrs(ctx)
    
    if objAttrs != nil {
    
      //DELETE
          log.Debugf(ctx, "",objAttrs)
          fmt.Fprintln(w,"Deleting previous file: " + theFileName)

         if err := bkt.Object(theFileName).Delete(ctx); err != nil {
              fmt.Fprintln(w,"Error deleting file...")
            }
        
      

    } else {
      fmt.Fprintln(w, "Creating new file " + theFileName + "...")

    }
    

    //DEBUG
    //fmt.Fprintln(w,splitURL[3])
    //fmt.Fprintln(w,prevObj)]
    //fmt.Fprintln(w, "Creating file " + "homepage.html...")
    //fmt.Fprintln(w,staticHTML)
    //log.Debugf(ctx, "",staticHTML)


    
     wc := bkt.Object(theFileName).NewWriter(ctx)
      wc.ContentType = "text/html"
      wc.Metadata = map[string]string{
        "x-goog-meta-foo": "foo",
        "x-goog-meta-bar": "bar",
      }

      fileWrite, errFW := wc.Write([]byte(staticHTML))
   
      if errFW != nil {
        return
      }

      wc.Close()

      log.Debugf(ctx, "",fileWrite)
      fmt.Fprintln(w,"Success writing static file!")
    




 //END POST
}


  //CLOSE
    defer client.Close()    

//END CLASS FUNC
}






/*
func getCloudContext(r *http.Request)(context.Context, error) {
  jsonKey,err := ioutil.ReadFile("../" + jsonKeyName)  

  if err != nil {
    return nil, err
  }

  conf, err := google.JWTConfigFromJSON(
      jsonKey,
      storage.ScopeFullControl,
    )

  if err != nil {
    return nil, err
  }

  ctx := appengine.NewContext(r)
  hc := conf.Client(ctx)

  return cloud.NewContext(aeID,hc),nil

//END FUNC
}



func (d *cloudItem) errorf(format string, args ...interface{}) {
  d.failed = true
  fmt.Fprintln(d.w, fmt.Sprintf(format, args...))
  log.Errorf(d.ctx, format, args...)
}

func (d *cloudItem) listBucket() {
  io.WriteString(d.w, "\nListbucket result:\n")

  query := &storage.Query{} //Prefix: "foo"
  it := d.bucket.Objects(d.ctx, query)
  for {
    obj, err := it.Next()
    if err == iterator.Done {
      break
    }
    if err != nil {
      d.errorf("listBucket: unable to list bucket %q: %v", d.bucketName, err)
      return
    }
    d.dumpStats(obj)
  }
}*/
/*
func (d *cloudItem) dumpStats(obj *storage.ObjectAttrs) {
  fmt.Fprintf(d.w, "(filename: /%v/%v, ", obj.Bucket, obj.Name)
  fmt.Fprintf(d.w, "ContentType: %q, ", obj.ContentType)
  fmt.Fprintf(d.w, "ACL: %#v, ", obj.ACL)
  fmt.Fprintf(d.w, "Owner: %v, ", obj.Owner)
  fmt.Fprintf(d.w, "ContentEncoding: %q, ", obj.ContentEncoding)
  fmt.Fprintf(d.w, "Size: %v, ", obj.Size)
  fmt.Fprintf(d.w, "MD5: %q, ", obj.MD5)
  fmt.Fprintf(d.w, "CRC32C: %q, ", obj.CRC32C)
  fmt.Fprintf(d.w, "Metadata: %#v, ", obj.Metadata)
  fmt.Fprintf(d.w, "MediaLink: %q, ", obj.MediaLink)
  fmt.Fprintf(d.w, "StorageClass: %q, ", obj.StorageClass)
  if !obj.Deleted.IsZero() {
    fmt.Fprintf(d.w, "Deleted: %v, ", obj.Deleted)
  }
  fmt.Fprintf(d.w, "Updated: %v)\n", obj.Updated)
}*/

