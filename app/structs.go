package app

import (
	//"reflect"

  "appengine"
 // "appengine/datastore"
  "html/template"
  "time"
)

//https://cloud.google.com/appengine/docs/standard/go/modules/runtime#Stats
//RUNTIME STATS
type Statistics struct {
    // CPU records the CPU consumed by this instance, in megacycles.
    CPU struct {
        Total   float64
        Rate1M  float64 // consumption rate over one minute
        Rate10M float64 // consumption rate over ten minutes
    }
    // RAM records the memory used by the instance, in megabytes.
    RAM struct {
        Current    float64
        Average1M  float64 // average usage over one minute
        Average10M float64 // average usage over ten minutes
    }
}


//IMAGE STRUCTS
type ServingURLOptions struct {
    Secure bool // whether the URL should use HTTPS

    // Size must be between zero and 1600.
    // If Size is non-zero, a resized version of the image is served,
    // and Size is the served image's longest dimension. The aspect ratio is preserved.
    // If Crop is true the image is cropped from the center instead of being resized.
    Size int
    Crop bool
}


//BLOB STRUCTS
type BlobUploadSession struct {
    BlobKey      appengine.BlobKey
    ContentType  string    `datastore:"content_type"`
    CreationTime float64     `datastore:"creation"`
    Filename     string    `datastore:"filename"`
    Hash		 string	   `datastore:"md5_hash"`
    Size         int64     `datastore:"size"`
    User		 string	   `datastore:"user"`
    SuccessPath  string    `datastore:"success_path"`
    State		 string	   `datastore:"state"`
    MaxBytes	 string	   `datastore:"max_bytes_total"`
    MaxBytesPB   string    `datastore:"max_bytes_per_blob"`

}

type BlobInfo struct {
	BlobKey      appengine.BlobKey
    ContentType  string    `datastore:"content_type"`
    CreationTime time.Time `datastore:"creation"`
    Filename     string    `datastore:"filename"`
    Size         int64     `datastore:"size"`
    MD5          string    `datastore:"md5_hash"`
    UploadID	string		`datastore:"upload_id"`

    // ObjectName is the Google Cloud Storage name for this blob.
    ObjectName string `datastore:"gs_object_name"`
}




//PAGE
type Page struct {
  Slug string			`json:"slug"`
  Title string			`json:"title"`
  PageTitle string		`json:"pagetitle"`
  Description string	`json:"description"`
  Keywords string		`json:"keywords"`
  Content string 		`datastore:",noindex" json:"content"`
  JS string				`json:"js"`
  CSS string			`json:"css"`
  Template string		`json:"template"`
  Single string			`json:"single"`
  //List []map[string]string
}

//STATIC PAGE
type StaticPage struct {
  URL string			`json:"url"`
  Title string			`json:"title"`
  Description string	`json:"description"`
  Keywords string		`json:"keywords"`
  Content string 		`datastore:",noindex" json:"content"`
  Template string		`json:"template"`
  Single string			`json:"single"`

}



type Content struct {
	List []map[string]string
	Data map[string]string
	Globals map[string]string
}


type Core struct {
	Core_Header string 		`datastore:",noindex" json:"core_header"`
  	Core_Footer string 		`datastore:",noindex" json:"core_footer"`
  	Core_Version string		`json:"core_version"`
  	Last_Update time.Time
}

type Style struct {
	Style_Name string 		`json:"style_name"`
  	Style_Sheet string 		`datastore:",noindex" json:"style_sheet"`
  	Last_Update time.Time
}


type Settings struct {
	Last_Update time.Time

	Site_Title string	`json:"site_title"`
	Site_Closed string	`json:"site_closed"`
	Site_Online string `json:"site_online"`
	Site_Analytics string `json:"site_analytics"`
	Site_Parser string

	//Home_Title string	`json:"home_title"`
	//Home_Desc string	`json:"home_desc"`
	//Home_KeyW string	`json:"home_keyw"`

	Cache_Control string `json:"cache_control"`
	Cache_Days string 		 `json:"cache_days"`
	Cache_Months string 	 `json:"cache_months"`
	
	Soon_URL string		`json:"soon_url"`
	Admin_Email string	`json:"admin_email"`



	//Soon_Title string	`json:"soon_title"`
	//Soon_Desc string	`json:"soon_desc"`
	//Soon_KeyW string	`json:"soon_keyw"`
	//From_Email string	`json:"from_email"`
	//To_Email string 	`json:"to_email"`
}



type SetImages struct {
	Img_Desktop int
	Img_Tablet int
	Img_Mobile int
	Img_Large int
	Img_Medium int
	Img_Small int
	Img_Icon int
	Img_Mini int
	Img_Micro int
}


//VIEW STRUCTS
type View struct {
	HTML map[string]template.HTML
    Data map[string]string
    List []map[string]map[string]string
    //Format func(string)string
   // Menus Menu
    //Site map[string]string
}

type xmlView struct {
	HTML map[string]template.HTML
    Data map[string]string
    Pages []string
}

type cssView struct {
	CSS map[string]template.HTML
}

//FORMS
type Forms struct {
	//Key string			`json:"key"`
	Title string			`json:"title"`
	TemplateList  string 
	TemplateItem  string 
	Fields []FieldType      `json:"fields"`

}

type FormFieldType struct {
	Name string
	//Label string
	UI string
	//Value string
	//Errors string
}


//DATA STRUCTS
type DataType struct {
	//Key string			`json:"key"`
	Title string			`json:"title"`
	URL string				`json:"typeurl"`
	Description string
	Keywords string
	TemplateList  string 
	TemplateItem  string 
	Fields []FieldType      `json:"fields"`

}

type FieldType struct {
	Name string
	//Label string
	UI string
	Order string
	//Value string
	//Errors string
}

type DataItem struct {
	Content []byte //[]FieldItem //map[string]string
	Type string
	Tags []string
	Title string
	URL  string
	TypeURL string
	Description string
	Keywords string
	DateOrder	time.Time //`datastore:"date_order"`
	Published bool
	//CreationTime time.Time //`datastore:"creation"`

}

type DataTemplate struct {
	TPL map[string]template.HTML
}

